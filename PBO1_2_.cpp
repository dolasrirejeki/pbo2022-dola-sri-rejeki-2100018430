#include <iostream>
using namespace std;

int main(){
	int nilai;
    char a[4]={'d','o','l','a'};   
    int x[2][2] = {{0,1}, {0,2}};
    int y;
    
    cout << "Masukkan Nilai : ";
	cin >> nilai;		       			   	// Input
    
    cout<<endl<<endl;
    for(int i=0; i<4; i++){								// Perulangan For + array 1D
        cout << "index ke-" << i << " = " << a[i] << endl;	// Output
    }
    
                                      			
    for(int i=0;i<2;i++){              // Array Multidimensi
		for(int j=0;j<2;j++){
			cout << x[i][j] << "\t";
		}
		cout << endl;
	}
    cout<<endl<<endl;
   if (nilai%2==0){										// if atau if else
        cout << nilai << " merupakan bilangan genap" << endl;
    }else{
        cout << nilai << " merupakan bilangan ganjil" << endl;
    }
    
    cout<<endl<<endl;                 
    
    while (y <= 5) {										//While
        cout << y << endl;
        y++;
    }
    
   do {                                        //do while
		cout<<y<<endl;
		y++;
	
	} while(y <= 10);                                       
	cout<<"Nilai terakhir y = "<<y<<endl;
	cout<<endl;
}
