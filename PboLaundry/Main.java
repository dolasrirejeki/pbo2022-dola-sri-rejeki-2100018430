public class Main {
    public static void main(String[] args) {
        pelanggan pg = new pelanggan();
        pakaian pk = new pakaian();
        laundry l = new laundry();

        pg.setId(001);
        pg.setNama("Dola Sri Rejeki");
        pg.setAlamat("Sleman, Yogyakarta");
        pk.setBanyakpakaian(25);
        pk.setTotal(75000);

        l.laundry(pg, pk);
        l.lihatdatapelanggan();
    }
}