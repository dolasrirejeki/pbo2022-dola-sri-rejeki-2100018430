public class pakaian {
    private int banyakpakaian;
    private int total;

    public int getBanyakpakaian() {
        return banyakpakaian;
    }

    public void setBanyakpakaian(int banyakpakaian) {
        this.banyakpakaian = banyakpakaian;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
